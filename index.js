
// Calculates most frequent numbers in a txt file 
//     and displays results in table
// Triggered on button click
function calculate() {

    // get how many of the most frequent numbers to find
    let TOP_NUMS = parseInt(document.getElementById("top_nums").value);
    // check value is valid
    if (isNaN(TOP_NUMS)) {
        updateErrorText("Error: Invalid input for 'Numbers to Get'."
            + " Value must be a number");
        return;
    } else if (TOP_NUMS < 1) {
        updateErrorText("Error: Input for 'Numbers to Get' cannot "
            + "be less than 1")
        return;
    }

    // open file and read
    const file = document.getElementById("input");
    let fr = new FileReader();
    try {
        fr.readAsText(file.files[0]);
    }
    catch (error) {
        if (error instanceof TypeError) {
            updateErrorText("TypeError: Invalid file type "
                + "or no file selected");
        } else {console.log(error.message);}
    }

    // Compute results and put in table
    // Triggered on load completion of file
    fr.onload = function() {
        // split text file into array
        let numbers = fr.result.split("\n");
        // count frequencies of numbers
        let frequencies = getFrequencies(numbers);
        // get most frequent numbers
        let most_frequent = getMostFrequent(frequencies, TOP_NUMS);
        
        // Check if array is empty
        // Else update table with results
        if (most_frequent.length == 0) {
            updateErrorText("Error: No numbers found in txt "
                + "file. Each line must only have one number");
        } else {
            updateResultsTable(most_frequent);
        }
    }

    // Check for errors in reading file
    fr.onerror = function() {
        console.log(fr.error);
    }
}


// Counts frequencies of numbers in an array.
// Returns an object of keys and their counts.
function getFrequencies(array) {
    let frequencies = {};
    for (let index=0; index<array.length; index++) {
        number = parseInt(array[index]);
        // check array element is int
        if (isNaN(number) == false) {
            // Checks if number exists in object and incriments it.
            // else initialises to 1.
            if (frequencies[number]) {
                frequencies[number]++;
            } else {
                frequencies[number] = 1;
            }
        }
    }
    return frequencies;
}


// Gets the keys of an object with greatest value.
// Moves object into an array, sorts it and slices it.
function getMostFrequent(frequencies, TOP_NUMS) {
    sortable_arr = Object.entries(frequencies);
    
    // sorts list descending, by 2nd index of each element
    sortable_arr.sort(([ ,count_1], [ ,count_2]) => count_2 - count_1);

    most_frequent = sortable_arr.slice(0, TOP_NUMS);
    return most_frequent;
}


// Updates a table with results of most frequent numbers
function updateResultsTable(most_frequent) {
    let table = document.getElementById("result_table");
    let error_text = document.getElementById("error_text");
    // Reset table and any error text
    table.innerHTML = "";
    error_text.innerHTML = "";

    // Create table headers and caption
    let caption = table.createCaption();
    caption.innerHTML = "Results";
    let thead = table.createTHead();
    let row = thead.insertRow(0);
    let cell_0 = row.insertCell(0);
    let cell_1 = row.insertCell(1);
    cell_0.innerHTML = "Number";
    cell_1.innerHTML = "Count";

    // Create table body and populate with results
    let tbody = table.createTBody();
    for (let index = 0; index < most_frequent.length; index++) {
        let row = tbody.insertRow(-1);
        let number_cell = row.insertCell(0);
        let count_cell = row.insertCell(1);
        number_cell.innerHTML = most_frequent[index][0];
        count_cell.innerHTML = most_frequent[index][1];
    }
}


// Updates error text element and resets table
function updateErrorText(text) {
    let table = document.getElementById("result_table");
    let error_text = document.getElementById("error_text");
    table.innerHTML = "";
    error_text.innerHTML = text;
}



